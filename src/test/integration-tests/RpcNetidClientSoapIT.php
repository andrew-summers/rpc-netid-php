<?php

use edu\wisc\doit\RpcNetidClientSoapConfig;
use edu\wisc\doit\RpcNetidClientSoap;
use edu\wisc\doit\RpcNetidStructQuestion;
use edu\wisc\doit\RpcNetidStructValidationResponse;

/**
 * Integration tests for edu\wisc\doit\RpcNetidClientSoap
 * 
 * WARNING: These tests connect to the Middleware NetID web service that you tell it to and mutate data. Make sure you understand all of the implications.
 * 
 * You will need to have an authentication certificate for the NetID web service. Additionally, you will need a static IP (WiscVPN recommended) that Middleware has allowed access to the web service.
 * 
 * A configuration file is required to specify test data for these integration tests. The file is looked for here: src/test/resources/integration-test-data.ini
 * 
 * A 'SAMPLE' file is included in the same directory to use as a template. Read the comments in that file carefully for successful testing.
 * 
 */

class RpcNetidClientSoapIT extends PHPUnit_Framework_TestCase {
	
	/** @var array */
	private static $testData = false;
	
	/** @var edu\wisc\doit\RpcNetidClientSoap */
	private static $client;
	
	/** @var string a username that is used in tests that are attempting to confirm behavior for a NetID username input that does not exist */
	private static $invalidUid = "__iamnotauser"; // This NetID should not exist in the UW-Madison identity system
	
	/** Sets up fixtures to be shared among all tests */
	public static function setUpBeforeClass() {
		
		$iniFile = realpath( __DIR__ . '/../resources/integration-test-data.ini' );
		if ( $iniFile === false ) { 
			throw new RuntimeException( "Cannot find integration test data: $iniFile" );
		}
		
		self::$testData = parse_ini_file( $iniFile, true );
		if ( self::$testData === false ) { 
			throw new RuntimeException( "Cannot parse integration test data: $iniFile" );
		}
		
		$config = new RpcNetidClientSoapConfig( 
				self::$testData['connection']['wsdl'],
				self::$testData['connection']['caCert'],
				self::$testData['connection']['clientCert']
		);

		self::$client = RpcNetidClientSoap::create( $config );
				
	}
	
	/** @test */
	public function getQuestions_validIT() {
		$result = self::$client->getQuestions( self::$testData['user']['uid'] );
		$this->assertInternalType( "array" , $result );
	}
	
	/** @test */
	public function getQuestions_invalidUidIT() {
		$result = self::$client->getQuestions( self::$invalidUid );
		$this->assertEmpty( $result );
	}
	
	/** @test */
	public function lockStatus_validIT() {
		$result = self::$client->lockStatus( self::$testData['user']['uid'] );
		$this->assertInstanceOf( 'edu\wisc\doit\RpcNetidStructLockStatus', $result );
	}
	
	/** @test */
	public function lockStatus_invalidUidIT() {
		$result = self::$client->lockStatus( 'iamnotauser' );
		$this->assertInstanceOf( 'edu\wisc\doit\RpcNetidStructLockStatus', $result );
		$this->assertTrue( $result->getLocked() );
	}
	
	/** @test */
	public function checkLOA_Loa2valid() {
		$result = self::$client->checkLOA( 
				self::$testData['user']['uid'],
				new DateTime( self::$testData['user']['birthdate'] ),
				intval( self::$testData['user']['wiscard'] ) );
		$this->assertTrue( $result->getIsValid() );
	}
	
	/** @test test valid credentials for an LOA1 NetID (NetID and DOB only) */
	public function checkLOA_Loa1Valid() {
		$result = self::$client->checkLOA(
			self::$testData['loa1-user']['uid'],
			new DateTime( self::$testData['loa1-user']['birthdate'] )
		);
		$this->assertTrue( $result->getIsValid() );
	}
	
	/** @test */
	public function checkLOA_invalid() {
		$result = self::$client->checkLOA(
				self::$testData['invalid-user']['uid'],
				new DateTime( self::$testData['invalid-user']['birthdate'] ),
				intval( self::$testData['invalid-user']['wiscard'] ) );
		$this->assertFalse( $result->getIsValid() );
	}
	
	/** @test Returns invalid if LOA2 and no wiscard provided */
	public function checkLOA_Loa2_no_wiscard() {
		$result = self::$client->checkLOA(
				self::$testData['user']['uid'],
				new DateTime( self::$testData['user']['birthdate'] ) );
		$this->assertFalse( $result->getIsValid() );
		$this->assertContains( RpcNetidStructValidationResponse::REASON_NEEDS_WISCARD, $result->getReasons() );
	}
	
	/** @test returns valid if LOA1 enters wiscard even if one doesn't exist */
	public function checkLOA_loa1_with_wiscard_returns_valid() {
		$result = self::$client->checkLOA(
				self::$testData['loa1-user']['uid'],
				new DateTime( self::$testData['loa1-user']['birthdate'] ),
				intval( 12345678901 )
		);
		$this->assertTrue( $result->getIsValid() );
	}
	
	/** @test */
	public function changePassword_validPasswordIT() {
		$result = self::$client->changePassword( self::$testData['user']['uid'], self::$testData['user']['password'] );
		$this->assertTrue( $result );
	}
	
	/** @test */
	public function changePassword_invalidUserIT() {
		$result = self::$client->changePassword( self::$invalidUid, self::$testData['user']['password'] );
		$this->assertFalse( $result );
	}
	
	/** @test */
	public function checkAnswers_validAnswersIT() {
		
		$questions = array();
		for( $i = 0; $i < sizeof( self::$testData['questions']['question'] ); $i++ ) {
			$q = new RpcNetidStructQuestion( 
					intval( self::$testData['questions']['number'][$i] ), 
					self::$testData['questions']['question'][$i],
					self::$testData['questions']['answer'][$i]);
			$questions[] = $q;
		}
		
		$result = self::$client->checkAnswers( 
				self::$testData['user']['uid'], 
				$questions,
				self::$testData['user']['ip'] );
		
		$this->assertTrue( $result );
		
	}
	
	/** @test */
	public function checkAnswers_invalidAnswersIT() {
		
		$questions = array();
		for( $i = 0; $i < sizeof( self::$testData['invalid-questions']['question'] ); $i++ ) {
			$q = new RpcNetidStructQuestion(
					intval( self::$testData['invalid-questions']['number'][$i] ),
					self::$testData['invalid-questions']['question'][$i],
					self::$testData['invalid-questions']['answer'][$i]);
			$questions[] = $q;
		}
		
		$result = self::$client->checkAnswers(
				self::$testData['invalid-user']['uid'],
				$questions,
				self::$testData['invalid-user']['ip'] );
		
		$this->assertFalse( $result );
		
	}
	
	/** @test */
	public function checkAnswers_invalidUidIT() {
	
		$questions = array();
		for( $i = 0; $i < sizeof( self::$testData['questions']['question'] ); $i++ ) {
			$q = new RpcNetidStructQuestion(
					intval( self::$testData['questions']['number'][$i] ),
					self::$testData['questions']['question'][$i],
					self::$testData['questions']['answer'][$i]);
			$questions[] = $q;
		}
	
		$result = self::$client->checkAnswers(
				self::$invalidUid,
				$questions,
				self::$testData['user']['ip'] );
	
		$this->assertFalse( $result );
	
	}
	
	/** @test */
	public function setRecoveryEmail_validIT() {
		$result = self::$client->setRecoveryEmail( self::$testData['user']['uid'], self::$testData['user']['recovery_email'] );
		$this->assertTrue( $result );
	}
	
	/** @test */
	public function setRecoveryEmail_invalidUidIT() {
		$result = self::$client->setRecoveryEmail( self::$invalidUid, self::$testData['user']['recovery_email'] );
		$this->assertFalse( $result );
	}
	
	/** 
	 * @test 
	 * @expectedException DomainException
	 */
	public function setRecoveryEmail_invalidEmail() {
		self::$client->setRecoveryEmail( self::$testData['user']['uid'], "notanemail" );
	}
	
	/** 
	 * Tests to make sure no two NetIDs can share the same Recovery Email
	 * @test 
	 */
	public function setRecoveryEmail_duplicates() {
		$result = self::$client->setRecoveryEmail( self::$testData['user']['uid'], self::$testData['user']['recovery_email'] );
		$this->assertTrue( $result );
		$result = self::$client->setRecoveryEmail( self::$testData['misc']['validuser'], self::$testData['user']['recovery_email'] );
		$this->assertFalse( $result );
	}
	
	/** @test */
	public function getRecoveryEmail_validIT() {
		$result = self::$client->getRecoveryEmail( self::$testData['user']['uid'] );
		$this->assertEquals( self::$testData['user']['recovery_email'], $result );
	}
	
	/** @test */
	public function getRecoveryEmail_invalidUidIT() {
		$result = self::$client->getRecoveryEmail( self::$invalidUid );
		$this->assertFalse( $result );
	}
	
	/** @test */
	public function getNetidForRecoveryEmail_validIT() {
		$result = self::$client->getNetidForRecoveryEmail( self::$testData["user"]["recovery_email"] );
		$this->assertEquals( self::$testData['user']['uid'], $result );
	}
	
	/** @test */
	public function passwordChoicePolicyCheck_validPassword() {
		$result = self::$client->passwordChoicePolicyCheck( self::$testData['user']['password'] );
		$this->assertTrue( $result->getIsValid() );
	}
	
	/** @test */
	public function passwordChoicePolicyCheck_invalidPassword() {
		$result = self::$client->passwordChoicePolicyCheck( self::$testData['invalid-user']['password'] );
		$this->assertFalse( $result->getIsValid() );
		$this->assertNotEmpty( $result->getReasons() );
	}

	/** @test */
	public function ping_control() {
		$result = self::$client->ping();
		$this->assertNotEmpty( $result );
	}
	
}

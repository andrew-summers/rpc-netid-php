<?php

namespace edu\wisc\doit;

/**
 * Stores configuration options creates a SOAP client for use in {@link edu\wisc\doit\RpcNetidClientSoap}
 */

class RpcNetidClientSoapConfig {
	
	/** @var string  path to client certificate for authentication. Must be in PEM format, containing public certificate and private key */
	private $clientCert;
	
	/** @var string  absolute path to the CA certificate that signs the endpoint certificate */
	private $caCert;
	
	/** @var string  absolute file to the WSDL */
	private $wsdl;

	/**
	 * 
	 * @param $wsdl the absolute path or URL to the WSDL
	 * @param $caCert the absolute path on the filesystem to the CA certificate that signs the end
	 * @param $clientCert the absolute path on the filesystem to the client certificate
	 */
	function __construct($wsdl, $caCert, $clientCert) {
		$this->setCaCert( $caCert  );
		$this->setClientCert( $clientCert );
		$this->setWsdl( $wsdl );
	}
	
	/**
	 * @return string  path to client certificate
	 */
	function getClientCert() { return $this->clientCert; }
	
	/**
	 * @param string $clientCert  path to client certificate
	 */
	function setClientCert( $clientCert ) { $this->clientCert = $clientCert; }
	
	/**
	 * @return string  path to CA certificate
	 */
	function getCaCert() { return $this->caCert; }
	
	/**
	 * @param string $caCert  path to CA certificate
	 */
	function setCaCert( $caCert ) { $this->caCert = $caCert; }
	
	/**
	 * @return string  path to WSDL (local file)
	 */
	function getWsdl() { return $this->wsdl; }
	
	/**
	 * @param string $wsdl  path to  WSDL (local file)
	 */
	function setWsdl( $wsdl ) { $this->wsdl = $wsdl; }
	
}

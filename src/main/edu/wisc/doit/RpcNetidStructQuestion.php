<?php

namespace edu\wisc\doit;

/**
 * Represents a security question for NetID password recovery
 */

class RpcNetidStructQuestion {
	
	/** @var int  question number */
	private $number;
	
	/** @var string  question */
	private $question;
	
	/** @var string  answer to question */
	private $answer;
	
	/**
	 * @param int $number
	 * @param string $question 
	 * @param string $answer
	 */
	function __construct( $number, $question, $answer = "" ) {
		$this->setNumber( $number );
		$this->setQuestion( $question );
		$this->setAnswer( $answer );
	}
	
	/**
	 * @return int  the question number
	 */
	function getNumber() { return $this->number; }
	
	/**
	 * @param int $number  the question number
	 * @throws DomainException
	 */
	function setNumber( $number ) {
		if ( is_int( $number ) !== true ) { throw new \DomainException("number must be an integer"); }
		$this->number = $number;
	}
	
	/**
	 * @return string  the question
	 */
	function getQuestion() { return $this->question; }
	
	/**
	 * @param string $question  the question
	 * @throws DomainException
	 */
	function setQuestion( $question ) { 
		if ( is_string( $question ) !== true ) { throw new \DomainException("question must be a string"); }
		$this->question = $question;
	}
	
	/**
	 * @return string  the answer
	 */
	function getAnswer() { return $this->answer; }
	
	/**
	 * @param string $answer  the answer
	 * @throws DomainException
	 */
	function setAnswer( $answer ) {
		if ( is_string( $answer ) !== true ) { throw new \DomainException( "answer must be a string" ); }
		$this->answer = $answer;
	}
	
}

<?php
/**
 * CertificateService class and exception
 */

namespace edu\wisc\doit;

/**
 * Helper class to manipulate and validate certificates
 */
class CertificateService {
	
	/**
	 * Validates an x509 certificate by determining if it can be parsed by {@link openssl_x509_parse}
	 * 
	 * @param string $cert path to certificate
	 * @throws edu\wisc\doit\CertificateServiceException
	 * @throws UnexpectedValueException
	 * @return boolean  true if the certificate is in a valid format
	 */
	
	public static function validateCertificate( $cert ) {
	
		// Validate path to certificate
		if ( empty( $cert ) or is_readable( $cert ) !== true ) {
			throw new CertificateServiceException("Unable to read certificate: $cert",
					CertificateServiceException::IO_ERROR );
		}
	
		// Attempt to parse certificate
		$certContents = file_get_contents( $cert );
		if ( $certContents === false ) {
			throw new \UnexpectedValueException("Unable to open file: $cert");
		}
	
		$x509 = openssl_x509_parse( $certContents );
	
		if ( $x509 === false or is_array( $x509 ) === false ) {
			throw new CertificateServiceException( "Cannot parse client certificate from: $cert", CertificateServiceException::CERT_ERROR );
		}
		
		return true;
	
	}
	
	/**
	 * Validates a private key by determining if it can be parsed by {@link openssl_pkey_get_details}
	 * @param string $path  path to certificate, which must be in a PEM format and *may* contain the public certificate
	 * @throws edu\wisc\doit\CertificateServiceException
	 * @return boolean  true if the private key is in a valid format
	 */
	public static function validatePrivateKey( $path ) {
	
		// Validate path to certificate
		if ( empty( $path ) or is_readable( $path ) !== true ) {
			throw new CertificateServiceException("Unable to read certificate: $path",
					CertificateServiceException::IO_ERROR );
		}
	
		// Attempt to parse private key from PEM file
		$key = openssl_pkey_get_private( "file://$path" );
		if ( $key === false ) {
			throw new CertificateServiceException("Unable to load private key from certificate: '$path'. Verify you are providing an unencrypted PEM file with both certficate and private key.",
					CertificateServiceException::CERT_ERROR );
		}
	
		$keyDetails = openssl_pkey_get_details( $key );
		if ( $keyDetails === false or is_array( $keyDetails ) === false ) {
			throw new CertificateServiceException("Unable to load private key from certificate: '$path'. Verify you are providing an unencrypted PEM file with both certficate and private key.",
					CertificateServiceException::CERT_ERROR );
		}
		
		return true;
	
	}
	
	/**
	 * Creates a PEM encoded certificate for a given public certificate and private key. 
	 * 
	 * <p><b>WARNING:</b> the resulting certificate is NOT encrypted. The private key will be in plain text
	 * @param string $cert  path to certificate
	 * @param string $key  path to private key, which must be unencrypted
	 * @param string $pemPath  path to save the generate PEM
	 * @throws RuntimeException
	 * @throws edu\wisc\doit\CertificateServiceException
	 * @return boolean  true if the certificate was created
	 */
	public static function createPem( $cert, $key, $pemPath ) {
		
		// Validate certificate
		self::validateCertificate( $cert );
		
		// Validate key
		self::validatePrivateKey( $key );
		
		// Get certificate string
		$certContents = file_get_contents( $cert );
		if ( $certContents === false ) {
			throw new \RuntimeException( "Unable to read contents from $cert" );
		}
		
		// Get key string
		$keyContents = file_get_contents( $key );
		if ( $keyContents === false ) {
			throw new \RuntimeException( "Unable to read contents from $key" );
		}
		
		$pemContents = trim( $certContents ) . "\n" . trim( $keyContents );
		
		$result = file_put_contents( $pemPath, $pemContents );
		if ( $result === false ) {
			throw new CertificateServiceException( "Unable to writen PEM certificate to: $pemPath", CertificateServiceException::IO_ERROR );
		}
		
		return true;
	
	}
	
}

/**
 * Exception class for {@link CertificateService}
 */
class CertificateServiceException extends \Exception {
	const IO_ERROR = 100;
	const CERT_ERROR = 101;
}

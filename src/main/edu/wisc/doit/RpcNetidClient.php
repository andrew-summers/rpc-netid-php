<?php namespace edu\wisc\doit;

/**
 * A PHP client for the NetID web service provided by DoIT Middleware.
 */

interface RpcNetidClient {
	
	/**
	 * Returns the NetID password recovery security questions for the given user.
	 * 
	 * In the case of the NetID not existing, an empty array will be returned. However, an empty array does NOT
	 * guarantee the NetID doesn't exist.
	 * 
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @return RpcNetidStructQuestion[]  array containing security questions for the user
	 */
	function getQuestions( $uid );
	
	/**
	 * Sends a response code to indicate the lock status of the given user
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @return RpcNetidStructLockStatus  an object with a status to indicate if locked and the reason
	 */
	function lockStatus ( $uid );

	/**
	 * Changes the password for a given NetID
	 * 
	 * <p><b>WARNING READ THIS IMPORTANT DO NOT SKIP:</b> This method DOES NOT check if the password
	 * meets complexity requirements. You MUST check the complexity using {@link RpcNetidClient::passwordChoicePolicyCheck}</p>
	 * 
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @param string $password  the password to be checked for policy adherence and replace current password
	 * @return bool  true if password was changed successfully, false otherwise
	 * @throws RpcNetidClientSoapException if unexpected response code from SOAP service
	 */
	function changePassword ( $uid, $password );
	
	/**
	 * Validates the answers to the NetID security questions for a given user
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @param RpcNetidStructQuestion[] $questions  array containing security questions for the user (Number and Answer required)
	 * @param string $ip  ip address of the user
	 * @return bool  true if the answers are correct, false otherwise
	 * @throws RpcNetidClientSoapException if unexpected response from web service
	 */
	function checkAnswers ( $uid, array $questions, $ip );
	
	/**
	 * Retrieves the recovery email attached to the given user's uid
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @return string|false  returns the recovery email on success, false if it was not found
	 */
	function getRecoveryEmail ( $uid );
	
	/**
	 * Retreieves the netid associated with the given user's recovery email address
	 * @param string $recoveryEmail  Recovery email address attached to a netid user
	 * @return string|false  the NetID associated with the recovery email address, or false if NetID could not be found
	 */
	function getNetidForRecoveryEmail ( $recoveryEmail );
	
	/**
	 * Sets recovery email for given uid and returns true if it was set correctly
	 * 
	 * @param string $uid  the uid of the user to search for (typically the NetID)
	 * @param string $recoveryEmail  recovery email address desired for the given user
	 * @return bool  true if the recovery email was set correctly, false if unable to set
	 * @throws DomainException  if email is invalid
	 */
	function setRecoveryEmail( $uid, $recoveryEmail );
	
	/**
	 * Validates a NetID password meets complexity requirements
	 * 
	 * @param string $password  the password
	 * @return RpcNetidStructValidationResponse  indicates if valid and the reason for invalid
	 */
	function passwordChoicePolicyCheck( $password );
	
	/**
	 * Verifies the identity of a UW-Madison user using his or her NetID, date of birth, 
	 * and optional Wiscard number.
	 * 
	 * <p>This method will take into account the user's LOA (level of assurance). If the user
	 * is LOA2 or higher, the WisCard number is required. Otherwise, it is ignored.</p>
	 * 
	 * <p>The method returns a {@link RpcNetidStructValidationResponse} object. If 
	 * {@link RpcNetidStructValidationResponse::getIsValid} returns true, the identity is verified.
	 * Otherwise, it will return false with zero or more reasons returned by
	 * {@link RpcNetidStructValidationResponse::getReasons}</p>
	 * 
	 * <p>If the user requires a Wiscard and one was not provided, the method will return a {@link RpcNetidStructValidationResponse}
	 * object with {@link RpcNetidStructValidationResponse::REASON_NEEDS_WISCARD} as one of the reasons in the array.</p>
	 * 
	 * @param string $uid  user's NetID
	 * @param \DateTime $birthdate  user's date of birth
	 * @param string $wiscard  user's 11-digit Wiscard number
	 * @return RpcNetidStructValidationResponse  validation response including reasons for failure.
	 */
	function checkLOA( $uid, \DateTime $birthdate, $wiscard = null );

	/**
	 * Returns the version of the NetID web service.
	 *
	 * @return string  the version of the web service
	 * @throws RpcNetidClientException  if web service responds with status code other than 200
	 */
	function ping();
	
}

/**
 * Exception class for {@link RpcNetidClient}
 */
class RpcNetidClientException extends \Exception {}

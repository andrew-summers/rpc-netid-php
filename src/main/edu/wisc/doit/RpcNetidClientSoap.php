<?php

/**
 * RpcNetidClientSoap class
 */

namespace edu\wisc\doit;

/**
 * SOAP implementation of NetID web services provided by DoIT Middleware.
 */

class RpcNetidClientSoap implements RpcNetidClient {
	
	/** @var SoapClient  PHP SoapClient*/
	private $soapClient;
	
	/**
	 * @param SoapClient $soapClient
	 */
	function __construct( \SoapClient $soapClient ) {
		$this->setSoapClient( $soapClient );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::getQuestions()
	 */
	public function getQuestions( $uid ) {
		
		if ( is_string( $uid ) !== true ) {
			throw new \DomainException( "uid parameter must be a string" );
		}
		
		$result = $this->getSoapClient()->getQuestions( array( 'uid' => $uid ) );
		
		if ( $result->result !== 200 ) {
			throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}", 
				RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
		}
		
		if ( isset( $result->Questions ) === false ) {
			throw new RpcNetidClientSoapException("Questions response not found",
				RpcNetidClientSoapException::UNEXPECTED_RESPONSE );
		}
		
		$questions = array();
		
		if ( isset( $result->Questions->QuestionPair ) === true ) {
			foreach( $result->Questions->QuestionPair as $curr ) {
				$question = new RpcNetidStructQuestion( $curr->Number, $curr->Question );
				$questions[] = $question;
			}
		}
		
		return $questions;
		
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::lockStatus()
	 */
	public function lockStatus( $uid ){
		
		if ( is_string( $uid ) !== true ) {
			throw new \DomainException( "uid parameter must be a string" );
		}
		
		$result = $this->getSoapClient()->lockStatus( array( 'uid' => $uid ) );
		
		if( $result->result === 200 ) {
			return new RpcNetidStructLockStatus(false, "Not locked out");
		}
		
		if( $result->result === 400 ){
			return new RpcNetidStructLockStatus(true, "Locked out of password reset - too many attempts");
		}
		
		if( $result->result === 402 ){
			return new RpcNetidStructLockStatus(true, "Locked out of password reset - security");
		}
		
		if( $result->result === 403 ){
			return new RpcNetidStructLockStatus(true, "Locked out of password reset - maintenance");
		}
		
		if( $result->result === 404 ){
			return new RpcNetidStructLockStatus(true, "Locked out of password reset - account is in deactivation, no grace");
		}
		
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::checkLOA()
	 */
	public function checkLOA( $uid, \DateTime $birthdate, $wiscard = null ) {
		
		if ( is_string( $uid ) !== true or empty( $uid ) === true  ) {
			throw new \InvalidArgumentException( "uid must be a nonempty string. Type given: " . gettype($uid) );
		}
		
		if ( is_null( $wiscard ) === false  ) {
			if ( is_string( $wiscard ) !== true ) {
				throw new \InvalidArgumentException( "wiscard must be a string. Type given: " . gettype($wiscard) );
			}
		}
		
		$parameters = array();
		$parameters['uid'] = strval( $uid );
		$parameters['birthdate'] = $birthdate->format("m-d-Y");
		if ( is_null( $wiscard ) === false ) { $parameters['cardid'] = $wiscard; }
		
		$result = $this->getSoapClient()->checkLOA( $parameters );
		
		if ( isset( $result->result ) !== true ) {
			throw new RpcNetidClientSoapException( "Web service did not return a result code", RpcNetidClientSoapException::UNEXPECTED_RESPONSE );
		}
		
		switch( $result->result ) {
			case 200:
				return new RpcNetidStructValidationResponse(true, array());
				break;
			case 400:
				return new RpcNetidStructValidationResponse(false, array());
				break;
			case 401:
				return new RpcNetidStructValidationResponse( false, array("Web service returned 401: invalid input
				parameters"));
				break;
			case 402:
				return new RpcNetidStructValidationResponse( false, array("Web service returned 402: no PVI found for
				 uid") );
				break;
			case 403:
				return new RpcNetidStructValidationResponse( false, array("Web service returned 403: no LOA found for
				 uid") );
				break;
			case 404:
				return new RpcNetidStructValidationResponse( false, array("Web service returned 404: no Wiscard
				eligibility data found for uid"));
				break;
			case 405:
				return new RpcNetidStructValidationResponse(false, array(RpcNetidStructValidationResponse::REASON_NEEDS_WISCARD ) );
				break;
		}
		
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
			RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
		
	}
			
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::changePassword()
	 */
	public function changePassword( $uid, $password ){
		
		if( is_string($uid) !== true ){
			throw new \DomainException("uid parameter must be a string");
		}
		
		if( is_string($password) !== true ){
			throw new \DomainException("password parameter must be a string");
		}
		
		$result = $this->getSoapClient()->changePassword( array( 'uid' => $uid, 'password' => $password ) );

		if( $result->result === 200 ){
			return true;
		}
		
		if( $result->result === 400 or $result->result === 401 ){
			return false;
		}
			
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
			RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );

	}
			
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::checkAnswers()
	 */
	public function checkAnswers( $uid, array $questions, $ip ){
		
		if( is_string($uid) !== true) {
			throw new \InvalidArgumentException("uid parameter must be a string");
		}

			
		if ( is_string( $ip ) !== true) {
			throw new \InvalidArgumentException("ip parameter must be a string");
		}
		
		if ( filter_var( $ip, FILTER_VALIDATE_IP ) === false ) {
			throw new \DomainException( "Invalid IP address: $ip" );
		}

		
		// Create array of parameters for the SOAP client
		$questionsParam = array( 'QuestionPair' => array() );
		
		foreach($questions as $question){
			
			if( is_a( $question, "edu\wisc\doit\RpcNetidStructQuestion" ) !== true ) {
				throw new \DomainException( "questions parameter must contain only RpcNetidStructQuestion" );
			}
			
			if ( empty( $question->getNumber() ) === true or empty( $question->getAnswer() ) === true ) {
				throw new \DomainException( "Number and Answer required for each RpcNetidStructQuestion in questions array" );
			}
			
			$questionsParam['QuestionPair'][] = array( 'Number' => $question->getNumber(), 'Answer' => $question->getAnswer() );
			
		}
		
		$result = $this->getSoapClient()->checkAnswers( array( 'uid' => $uid, 'ip' => $ip, 'Questions' => $questionsParam ) );
		
		if( $result->result === 200 ){
			return true;
		}
		
		if( $result->result === 400 ){
			return false;
		}
			
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
	
	}
	
		
	/**	
	 * @see \edu\wisc\doit\RpcNetidClient::getRecoveryEmail()
	 */
	public function getRecoveryEmail( $uid ){
		if( is_string($uid) !== true){
			throw new \InvalidArgumentException("uid parameter must be a string");
		}
		
		$result = $this->getSoapClient()->getRecoveryEmail( array( 'netid' => $uid ) );
		
		if( $result->result === 200){
			return $result->recoveryemail;
		}
		
		if ( $result->result === 204 ) {
			return false;
		}
		
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
		
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::getNetIDForRecoveryEmail()
	 */
	public function getNetidForRecoveryEmail( $recoveryEmail ){
		if( is_string($recoveryEmail) !== true ){
			throw new \InvalidArgumentException("recoveryEmail parameter must be a string");
		}
	
		$result = $this->getSoapClient()->getNetIDForRecoveryEmail( array( 'recovery_email' => $recoveryEmail ) );
	
		if( $result->result === 200 ){
			return $result->netid;
		}
	
		if( $result->result === 204){
			return false;
		}
	
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::passwordChoicePolicyCheck()
	 */
	public function passwordChoicePolicyCheck( $password ){
		if(is_string($password) !== true){
			throw new \DomainException("password parameter must be a string");
		}
		
		$result = $this->getSoapClient()->passwordChoicePolicyCheck( array('password' => $password) );
		
		if( $result->result === 200){
			return new RpcNetidStructValidationResponse(true, array() );
		}
		
		if( $result->result === 400){
			return new RpcNetidStructValidationResponse(false, $result->Reasons->Reason );
		}
		
		if( $result->result === 401){
			return new RpcNetidStructValidationResponse(false, $result->Reasons->Reason );
		}
		
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \edu\wisc\doit\RpcNetidClient::setRecoveryEmail()
	 */
	public function setRecoveryEmail( $uid, $recoveryEmail ){
		if(is_string($uid) !== true){
			throw new \DomainException("uid parameter must be a string");
		}
		
		if(is_string($recoveryEmail) !== true){
			throw new \DomainException("recoveryEmail parameter must be a string");
		}
		
		$result = $this->getSoapClient()->setRecoveryEmail( array('netid' => $uid, 'recoveryemail' => $recoveryEmail) );
		
		if( $result->result === 200 ){
			return true;
		}
		
		if ( $result->result === 422 ) {
			throw new \DomainException( "$recoveryEmail is an invalid email" );
		}
		
		if( $result->result === 502 ){
			return false;
		}
		
		throw new RpcNetidClientSoapException("Unexpected status code: {$result->result}",
		RpcNetidClientSoapException::UNEXPECTED_STATUS_CODE );
	}

	public function ping() {

		$result = $this->getSoapClient()->ping([]);

		if ( isset( $result->version ) ) {
			return $result->version;
		} else {
			throw new RpcNetidClientSoapException("version not returned by web service",
				RpcNetidClientSoapException::UNEXPECTED_RESPONSE);
		}

	}
	
	/**
	 * Creates a client initialized with the given configuration otpions
	 * @param RpcNetidClientSoapConfig $config  configuration options
	 * @return \edu\wisc\doit\RpcNetidClientSoap
	 */
	static function create( RpcNetidClientSoapConfig $config ) {
		
		// Validate certificates
		CertificateService::validateCertificate( $config->getClientCert() );
		CertificateService::validatePrivateKey( $config->getClientCert() );
		CertificateService::validateCertificate( $config->getCaCert() );
	
		// Configure SSL Context
		$sslOptions = array(
			'ssl' => array (
				'cafile' => $config->getCaCert(),
				'local_cert' => $config->getClientCert()
			)
		);
		$sslContext = stream_context_create( $sslOptions );
	
		// Configure SOAP options
		$soapOptions = array(
			'stream_context' => $sslContext
		);
	
		$soap = new \SoapClient( $config->getWsdl(), $soapOptions );
		return new RpcNetidClientSoap( $soap );
	
	}
	
	/**
	 * @return \SoapClient
	 */
	public function getSoapClient() { return $this->soapClient; }
	
	/**
	 * @param SoapClient $soapClient
	 */
	public function setSoapClient( \SoapClient $soapClient ) { $this->soapClient = $soapClient; }
	
}

/**
 * Exception class for {@link RpcNetidClientSoap}
 */
class RpcNetidClientSoapException extends RpcNetidClientException {
	const UNEXPECTED_STATUS_CODE = 100;   // web service returns status code that cannot be handled (runtime exception)
	const UNEXPECTED_RESPONSE = 101;  // web service returns content that is not expected
	const INVALID_EMAIL = 102;
}
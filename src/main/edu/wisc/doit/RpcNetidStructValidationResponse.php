<?php

namespace edu\wisc\doit;

/**
 * Represents a validation response. This can be used when information about why something
 * is invalid needs to be communicated.
 */

class RpcNetidStructValidationResponse {
	
	/** @var bool  true if valid, false if invalid */
	private $isValid;
	
	/** @var mixed[]  the reasons for being invalid */
	private $reasons;
	
	/**
	 * @var int  encoded reason for when a user needs a Wiscard number
	 */
	const REASON_NEEDS_WISCARD = 100;
	
	/**
	 * @param bool $isValid
	 * @param string $reason
	 */
	function __construct( $isValid, array $reasons ) {
		$this->setIsValid( $isValid );
		$this->setReasons( $reasons );
	}
	
	/**
	 * @return bool  true if valid, false if invalid
	 */
	function getIsValid() { return $this->isValid; }
	
	/**
	 * @param bool $isValid  true if valid, false if invalid
	 */
	function setIsValid( $isValid ){
		if( is_bool($isValid) !== true ) { throw new \DomainException("isValid must be a bool"); }
		$this->isValid = $isValid;
	}
	
	/**
	 * @return string  the reason for a password being invalid
	 */
	function getReasons() { return $this->reasons; }
	
	/**
	 * @param string[] $reason  the reason for a password being invalid
	 */
	function setReasons( array $reasons ){
		$this->reasons = $reasons;
	}
}

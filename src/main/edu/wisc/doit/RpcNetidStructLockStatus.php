<?php

namespace edu\wisc\doit;

/**
 * Represents a lock status of a user
 */

class RpcNetidStructLockStatus {
	
	/** @var bool  locked if true, unlocked if false */
	private $locked;
	
	/** @var string  reason for being locked */
	private $reason;
	
	
	/**
	 * @param bool $locked
	 * @param string $reason
	 */
	function __construct( $locked, $reason = "" ) {
		$this->setLocked( $locked );
		$this->setReason( $reason );
	}
	
	/**
	 * @return bool  locked if true, unlocked if false
	 */
	function getLocked() { return $this->locked; }
	
	/**
	 * @param bool $locked  locked if true, unlocked if false
	 * @throws DomainException
	 */
	function setLocked( $locked ){
		if ( is_bool( $locked ) !== true ) { throw new \DomainException("locked must be a bool"); }
		$this->locked = $locked;
	}
	
	/**
	 * @return string  reason for being locked
	 */
	function getReason() { return $this->reason; }
	
	/**
	 * @param string $reason reason for being locked
	 * @throws DomainException
	 */
	function setReason( $reason ){
		if ( is_string( $reason ) !== true ) { throw new \DomainException("reason must be a string"); }
		$this->reason = $reason;
	}
	
}
